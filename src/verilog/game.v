// File: vga_example.v
// This is the top level design for EE178 Lab #4.

// The `timescale diballive specifies what the
// simulation time units are (1 ns here) and what
// the simulator time step should be (1 ps here).

`timescale 1 ns / 1 ps

// Declare the module and its ports. This is
// using Verilog-2001 syntax.

module game (
  input wire clk,
  input wire rst,  
  input  PS2Data,
  input  PS2Clk,
  
  output reg vs,
  output reg hs,
  output reg [3:0] r,
  output reg [3:0] g,
  output reg [3:0] b,
  output wire pclk_mirror
  
  );

  
   wire pclk;
   
   (* KEEP = "TRUE" *) 
   (* ASYNC_REG = "TRUE" *)
   reg [7:0] safe_start = 0;
   
  clk_wiz_0 clk_wiz(    
   
     .reset(rst),
     .locked(),
     .clk(clk),
     
     .clk40MHz(pclk)
        
   );
 
 
   ODDR pclk_oddr (
     .Q(pclk_mirror),
     .C(pclk),
     .CE(1'b1),
     .D1(1'b1),
     .D2(1'b0),
     .R(1'b0),
     .S(1'b0)
   );
 
                               
    wire [10:0] vcount, hcount;  
    wire vsync, hsync;           
    wire vblnk, hblnk;           
                               
    wire [10:0] vcount_out_bg, hcount_out_bg;
    wire vsync_out_bg, hsync_out_bg;
    wire vblnk_out_bg, hblnk_out_bg;
    wire [11:0] rgb_out_bg;
    
    wire [10:0] vcount_out_ball, hcount_out_ball;   
    wire vsync_out_ball, hsync_out_ball;    
    wire vblnk_out_ball, hblnk_out_ball;
    wire [11:0] rgb_out_ball, rgb_out_char;
    wire [11:0] rgb_pixel;
    wire [11:0] rgb_addr;
    
    wire [11:0] xpos_in;
    wire [11:0] ypos_in;
    wire [11:0] xpos_sync;
    wire [11:0] ypos_sync;
      
    wire [3:0] red_out;
    wire [3:0] green_out;
    wire [3:0] blue_out;
    
    wire [2:0] arrow;
    wire [11:0] xpos_ctl;
    wire [11:0] ypos_ctl;
    
    wire [7:0] r_data;
    wire [3:0] level_in;
    wire [1:0] level_done; 
    wire level_clr, disp_clr, flag;
    
    wire [11:0] ball_init_x, ball_init_y;

    
  vga_timing my_timing (
    
    .vcount(vcount),    
    .vsync(vsync),          
    .vblnk(vblnk),          
    .hcount(hcount),        
    .hsync(hsync),          
    .hblnk(hblnk),          
    .pclk(pclk),            
    .rst(rst)  
  );
       
  draw_background my_background(
   
    .vcount_in(vcount),    
    .vsync_in(vsync),       
    .vblnk_in(vblnk),       
    .hcount_in(hcount),     
    .hsync_in(hsync),       
    .hblnk_in(hblnk),       
    .pclk(pclk),            
    .rst(rst), 
    .rgb_pixel(12'h4_c_f), 
    .level_in(level_in),          
    
    .vcount_out(vcount_out_bg),
    .vsync_out(vsync_out_bg),
    .vblnk_out(vblnk_out_bg),
    .hcount_out(hcount_out_bg),
    .hsync_out(hsync_out_bg),
    .hblnk_out(hblnk_out_bg),
    .rgb_out(rgb_out_bg),   
    .level_out(),
    .ball_init_x(ball_init_x),
    .ball_init_y(ball_init_y)

      
  );
  
  ball my_ball(
    
    .vcount_in(vcount_out_bg),   
    .vsync_in(vsync_out_bg),           
    .vblnk_in(vblnk_out_bg),           
    .hcount_in(hcount_out_bg),         
    .hsync_in(hsync_out_bg),           
    .hblnk_in(hblnk_out_bg),           
    .rgb_in(rgb_out_bg),                
    .rst(rst),                         
    .pclk(pclk),                       
    .xpos(xpos_ctl),                  
    .ypos(ypos_ctl),
    
                                 
    .vcount_out(vcount_out_ball),      
    .vsync_out(vsync_out_ball),        
    .vblnk_out(vblnk_out_ball),        
    .hcount_out(hcount_out_ball),      
    .hsync_out(hsync_out_ball),        
    .hblnk_out(hblnk_out_ball),        
    .rgb_out(rgb_out_ball)
    
         
  );
    
    

  ball_ctl my_ball_ctl(
    
    .rst(rst),
    .clk(pclk),
    .arrow_in(arrow), 
    .disp_clr(disp_clr),
    .ball_init_x(ball_init_x),
    .ball_init_y(ball_init_y),
    
    .xpos(xpos_ctl),    
    .level_done(level_done),
    .level_out(level_in),              
    .ypos(ypos_ctl)
   
  );
  
 
  
  keyboard my_keyboard(
    .rst(rst),
    .clk(pclk),
    .kclk(PS2Clk),
    .kdata(PS2Data),
    
    .arrow_out(arrow),
    .read_out(),
    .disp_clr(disp_clr)
   );
         
  
  
 
  message my_message(
        
    .vcount_in(vcount_out_ball),         
    .hcount_in(hcount_out_ball),          
    .rgb_in(rgb_out_ball),                
    .rst(rst),                         
    .pclk(pclk),  
    .level_done(level_done), 
    .disp_clr(disp_clr),                
                       
    .rgb_out(rgb_out_char)
    
  );
  

  
      
  always @(posedge pclk)begin
    
    hs <= hsync_out_ball;
    vs <= vsync_out_ball;
    {r,g,b}<=rgb_out_char;
         
  end


endmodule
