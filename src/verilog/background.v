`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04.08.2019 13:03:02
// Design Name: 
// Module Name: background
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module background(

    input wire [10:0] hcount_in,
    input wire hblnk_in,
    input wire hsync_in,
    input wire [10:0] vcount_in,
    input wire vblnk_in,
    input wire vsync_in,
    input wire pclk,
    input wire rst,
    input wire [11:0] rgb_pixel,
    input wire [3:0] level_in,

    output wire [10:0] hcount_out,
    output wire hblnk_out,
    output wire hsync_out,
    output wire [10:0] vcount_out,
    output wire vsync_out,
    output wire vblnk_out,
    output wire [11:0] rgb_out,
    //output wire [11:0] rgb_addr,
    output wire [3:0] level_out

    );
    
    wire [11:0] rgb_pixel;
    wire [11:0] rgb_addr;
    
    
    draw_background my_draw_background(
       
        .vcount_in(vcount_in),    
        .vsync_in(vsync_in),       
        .vblnk_in(vblnk_in),       
        .hcount_in(hcount_in),     
        .hsync_in(hsync_in),       
        .hblnk_in(hblnk_in),       
        .pclk(pclk),            
        .rst(rst), 
        .rgb_pixel(rgb_pixel), 
        .level_in(level_in),          
        
        .vcount_out(vcount_out),
        .vsync_out(vsync_out),
        .vblnk_out(vblnk_out),
        .hcount_out(hcount_out),
        .hsync_out(hsync_out),
        .hblnk_out(hblnk_out),
        .rgb_out(rgb_out),
        //.rgb_addr(rgb_addr),
        .level_out(level_out)
    
          
      );
      
      /*image_exit my_image_exit(
        .clk(pclk),
        .address(rgb_addr),
        .rgb(rgb_pixel)
        
        );*/
    
endmodule
