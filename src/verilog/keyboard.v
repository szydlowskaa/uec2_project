`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06.09.2019 20:34:04
// Design Name: 
// Module Name: keyboard
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module keyboard(

    input clk,
    input kclk,
    input kdata,
    input rst,
    
    output wire [2:0] arrow_out,    
    output wire disp_clr,
    output wire read_out

    );   
    
    
   wire [7:0] keycode;
   wire oflag;            
    
   PS2Receiver uut (
        .clk(clk),
        .kclk(kclk),
        .kdata(kdata),
        
        .keycode(keycode),
        .oflag(oflag)
      );
      
     
      
    keyboard_read my_keyboard_read(
          .reset(rst),
          .clk(clk),
          .flag(oflag),    
          .r_data(keycode),
          
          .arrow_out(arrow_out),
          .read_out(read_out),
          .disp_clr(disp_clr)
         );  
      
      
    
    
    
endmodule
