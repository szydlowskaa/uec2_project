`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 19.08.2019 19:54:45
// Design Name: 
// Module Name: ball_ctl
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ball_ctl(

    input wire clk,
    input wire rst,
    input wire [2:0] arrow_in,
    input wire disp_clr,
    input wire [11:0] ball_init_x, ball_init_y,
    
    output wire [1:0] level_done,
    output wire [11:0] xpos,
    output wire [3:0] level_out,
    output wire [11:0] ypos
);

 wire [10:0] x_pixel;
 wire [10:0] y_pixel;
 wire [11:0] rgb_in;
 wire [3:0] level_in;


 draw_ball_ctl my_draw_ball_ctl(
    
    .rst(rst),
    .clk(clk),
    .arrow_in(arrow_in),  
    .rgb_in(rgb_in),  
    .disp_clr(disp_clr),
    .level_in(level_in),
    .ball_init_x(ball_init_x),
    .ball_init_y(ball_init_y),
    
    .x_pixel(x_pixel),
    .y_pixel(y_pixel),
    .xpos(xpos),    
    .level_done(level_done),
    .level_out(level_out),              
    .ypos(ypos)
   
  );
  
 draw_background my_background_ctl(
       
    .vcount_in(y_pixel),    
    .vsync_in(0),       
    .vblnk_in(0),       
    .hcount_in(x_pixel),     
    .hsync_in(0),       
    .hblnk_in(0),       
    .pclk(clk),            
    .rst(rst), 
    .rgb_pixel(12'h002), 
    .level_in(level_out),     
       
    .ball_init_x(),
    .ball_init_y(),
    .vcount_out(),
    .vsync_out(),
    .vblnk_out(),
    .hcount_out(),
    .hsync_out(),
    .hblnk_out(), 
    .rgb_out(rgb_in),    
    .level_out(level_in)
 );




    
endmodule
