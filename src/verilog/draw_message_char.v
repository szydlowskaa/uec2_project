`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09.04.2019 17:57:04
// Design Name: 
// Module Name: draw_rect_char
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module draw_message_char(
    input wire [7:0] char_pixels,
    input wire [10:0] hcount_in,
    //input wire hblnk_in,               
    //input wire hsync_in,               
    input wire [10:0] vcount_in,       
    //input wire vblnk_in,               
    //input wire vsync_in,               
    input wire pclk,                   
    input wire rst,                    
    input wire [11:0] rgb_in,
    input wire [1:0] level_done, 
    input wire disp_clr,     
    
       
    output  wire [3:0] char_line,
    output  wire [9:0] char_xy,
    //output reg [10:0] hcount_out,
    //output reg hblnk_out,                  
    //output reg hsync_out,                  
    //output reg [10:0] vcount_out,          
    //output reg vsync_out,                  
    //output reg vblnk_out,                 
    output reg [11:0] rgb_out
                  
                                           
    
    );
    
    
      wire [5:0] addrx, addry;
      //reg hsync_nxt, vsync_nxt, hblnk_nxt, vblnk_nxt;
      reg [10:0] hcount_nxt, vcount_nxt;
      reg [7:0]  vcount_rect, hcount_rect;
      reg [11:0] rgb_nxt;
      //reg hsync_temp, vsync_temp, hblnk_temp, vblnk_temp,
      reg disp, disp_nxt;
      reg [10:0] hcount_temp, vcount_temp;
      reg [11:0] rgb_temp;
      reg level;
      reg [1:0] level_done_nxt;
      
    
   
    
      
      localparam HEIGHT = 32, WIDTH = 256, COLOUR= 12'hf_f_f, BG=12'hc_c_c, XPOS=264, YPOS=236, WIDTH_M=WIDTH+16, HEIGHT_M=HEIGHT+16, XPOS_M=XPOS - (WIDTH_M-WIDTH)/2, YPOS_M=YPOS - (HEIGHT_M-HEIGHT)/2, XPOS_INS=264, YPOS_INS=400 ;
      
      
   
      
      
  always @(posedge pclk) 
  
    if (rst) begin
        //hsync_out <= 0;
        //vsync_out <= 0;
        //hblnk_out <= 0;
        //vblnk_out <= 0;        
        //hcount_out <= 0;
        //vcount_out <= 0;
        rgb_out<=0;
        
            
    end
    else begin 
             
        vcount_temp<=vcount_in;
        hcount_temp<=hcount_in;               
        //vsync_temp<=vsync_in;                   
        //vblnk_temp<=vblnk_in;                   
        //hblnk_temp<=hblnk_in;                   
        //hsync_temp<=hsync_in;  
        rgb_temp<=rgb_in;                 
                
        
       // vcount_out<=vcount_nxt;
       // hcount_out<=hcount_nxt;
       // vsync_out<=vsync_nxt;
       // vblnk_out<=vblnk_nxt;
       // hblnk_out<=hblnk_nxt;
       // hsync_out<=hsync_nxt;
        rgb_out<=rgb_nxt;
        
        disp<=disp_nxt;
        
        level_done_nxt<=level_done;
        
      end
      
      
      always@* begin
      
        vcount_nxt=vcount_temp;
        hcount_nxt=hcount_temp;
        //vsync_nxt=vsync_temp;
        //vblnk_nxt=vblnk_temp;
        //hblnk_nxt=hblnk_temp;
        //hsync_nxt=hsync_temp;
        
        if(level_done_nxt==2'b11) begin    
            vcount_rect = vcount_in - YPOS_INS;
            hcount_rect = hcount_in - XPOS_INS;
            level=1;
        end
        else begin
            vcount_rect = vcount_in - YPOS;
            hcount_rect = hcount_in - XPOS;
            level=0;
        end
            
        if (level_done_nxt==2'b01||level_done_nxt==2'b11)begin            
            disp_nxt=1;
        end    
        else if (disp_clr) begin
            disp_nxt=0;
                  
        end    
        else begin
            disp_nxt=disp;
            
        end
        
        if(disp&&level_done_nxt==2'b01)begin
            
            if( hcount_nxt>=XPOS_M && hcount_nxt<XPOS_M+WIDTH_M && vcount_nxt>=YPOS_M && vcount_nxt<YPOS_M+HEIGHT_M )
               if(((hcount_nxt==XPOS_M||hcount_nxt==XPOS_M+WIDTH_M-1)&& vcount_nxt>=YPOS_M && vcount_nxt<YPOS_M+HEIGHT_M)||((vcount_nxt==YPOS_M||vcount_nxt==YPOS_M+HEIGHT_M-1)&& hcount_nxt>=XPOS_M && hcount_nxt<XPOS_M+WIDTH_M) )
                   rgb_nxt = COLOUR;
               else if(((hcount_nxt==XPOS_M+5||hcount_nxt==XPOS_M+WIDTH_M-5-1)&& vcount_nxt>=YPOS_M+5 && vcount_nxt<YPOS_M+HEIGHT_M-5)||((vcount_nxt==YPOS_M+5||vcount_nxt==YPOS_M+HEIGHT_M-5-1)&& hcount_nxt>=XPOS_M+5 && hcount_nxt<XPOS_M+WIDTH_M-5) )
                   rgb_nxt=COLOUR;
               else
                   rgb_nxt=BG;
            else   
               rgb_nxt=rgb_temp;   
                                  
        
            if( hcount_nxt>=XPOS && hcount_nxt<XPOS+WIDTH && vcount_nxt>=YPOS && vcount_nxt<YPOS+HEIGHT ) 
               if(char_pixels[4'b1000-hcount_nxt[3:0]-XPOS[3:0]])
                   rgb_nxt = COLOUR;
               else 
                   rgb_nxt=rgb_nxt;
               
            else 
               rgb_nxt=rgb_nxt;
        
           
           
        end  
        
        else if(disp&&level_done_nxt==2'b11)   
             if( hcount_nxt>=XPOS_INS && hcount_nxt<XPOS_INS+WIDTH && vcount_nxt>=YPOS_INS && vcount_nxt<YPOS_INS+HEIGHT ) 
                if(char_pixels[4'b1000-hcount_nxt[3:0]-XPOS_INS[3:0]])
                    rgb_nxt = COLOUR;
                else 
                    rgb_nxt=rgb_temp;
                
             else 
                rgb_nxt=rgb_temp;
        
        else
            rgb_nxt=rgb_temp;        
      
      end
      
   
                           
       assign char_line= vcount_rect[3:0];                     
       assign char_xy={level,vcount_rect[7:4], hcount_rect[7:3]};
      

  endmodule
    
    

