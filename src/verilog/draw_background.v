`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05.03.2019 14:49:59
// Design Name: 
// Module Name: draw_background
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module draw_background(

    input wire [10:0] hcount_in,
    input wire hblnk_in,
    input wire hsync_in,
    input wire [10:0] vcount_in,
    input wire vblnk_in,
    input wire vsync_in,
    input wire pclk,
    input wire rst,
    input wire [11:0] rgb_pixel,
    input wire [3:0] level_in,
    
    output reg [10:0] hcount_out,
    output reg hblnk_out,
    output reg hsync_out,
    output reg [10:0] vcount_out,
    output reg vsync_out,
    output reg vblnk_out,
    output reg [11:0] rgb_out,    
    output reg [3:0] level_out,
    output reg [11:0] ball_init_x,
    output reg [11:0] ball_init_y

    );
    
    reg [11:0] xpos,ypos;
    wire [5:0] addrx, addry;
   
    localparam BG_COLOUR=12'hc_c_c, WALL_COLOUR=12'hf_f_f, EXIT_SIZE=86;
    
    always @(posedge pclk) begin
    if (rst) begin
         rgb_out    <= 0;
         hcount_out <= 0;
         hblnk_out  <= 0;       
         hsync_out  <= 0;           
         vcount_out <= 0;    
         vsync_out  <= 0;           
         vblnk_out  <= 0;
         level_out  <= 0;  
              
    end
    else if (vblnk_in || hblnk_in) rgb_out <= 12'h0_0_0; 
       else begin
           //area 800x600
           if( hcount_in>=xpos && hcount_in<xpos+EXIT_SIZE && vcount_in>=ypos && vcount_in<ypos+EXIT_SIZE)  rgb_out <= rgb_pixel; 
           else if (vcount_in >= 0&&vcount_in<5) rgb_out <= WALL_COLOUR;           
           else if (vcount_in <= 599&& vcount_in>=595) rgb_out <= WALL_COLOUR;           
           else if (hcount_in>= 0&&hcount_in<5) rgb_out <= WALL_COLOUR;           
           else if (hcount_in <=799 && hcount_in>=795) rgb_out <= WALL_COLOUR;
           else 
              case(level_in) 
              4'b0000: begin
              
                   //WALLS
                   if(hcount_in<=100&&hcount_in>=50&&vcount_in>=200&&vcount_in<=205)rgb_out <= WALL_COLOUR;
                   else if(hcount_in<=200&&hcount_in>=150&&vcount_in>=100&&vcount_in<=105)rgb_out <= WALL_COLOUR;
                   else if(hcount_in<=200&&hcount_in>=150&&vcount_in>=150&&vcount_in<=155)rgb_out <= WALL_COLOUR;
                   else if(hcount_in<=300&&hcount_in>=250&&vcount_in>=100&&vcount_in<=105)rgb_out <= WALL_COLOUR;
                   else if(hcount_in<=300&&hcount_in>=250&&vcount_in>=150&&vcount_in<=155)rgb_out <= WALL_COLOUR;
                   else if(hcount_in<=300&&hcount_in>=250&&vcount_in>=200&&vcount_in<=205)rgb_out <= WALL_COLOUR;
                   else if(hcount_in<=450&&hcount_in>=400&&vcount_in>=100&&vcount_in<=105)rgb_out <= WALL_COLOUR;
                   else if(hcount_in<=450&&hcount_in>=400&&vcount_in>=150&&vcount_in<=155)rgb_out <= WALL_COLOUR;
                   else if(hcount_in<=550&&hcount_in>=500&&vcount_in>=150&&vcount_in<=155)rgb_out <= WALL_COLOUR;
                   else if(hcount_in<=750&&hcount_in>=700&&vcount_in>=100&&vcount_in<=105)rgb_out <= WALL_COLOUR;
                   else if(hcount_in<=55&&hcount_in>=50&&vcount_in>=100&&vcount_in<=150)rgb_out <= WALL_COLOUR;
                   else if(hcount_in<=55&&hcount_in>=50&&vcount_in>=150&&vcount_in<=200)rgb_out <= WALL_COLOUR;
                   else if(hcount_in<=155&&hcount_in>=150&&vcount_in>=100&&vcount_in<=150)rgb_out <= WALL_COLOUR;
                   else if(hcount_in<=155&&hcount_in>=150&&vcount_in>=150&&vcount_in<=200)rgb_out <= WALL_COLOUR;
                   else if(hcount_in<=205&&hcount_in>=200&&vcount_in>=100&&vcount_in<=150)rgb_out <= WALL_COLOUR;
                   else if(hcount_in<=205&&hcount_in>=200&&vcount_in>=150&&vcount_in<=200)rgb_out <= WALL_COLOUR;
                   else if(hcount_in<=255&&hcount_in>=250&&vcount_in>=100&&vcount_in<=150)rgb_out <= WALL_COLOUR;
                   else if(hcount_in<=255&&hcount_in>=250&&vcount_in>=150&&vcount_in<=200)rgb_out <= WALL_COLOUR;
                   else if(hcount_in<=305&&hcount_in>=300&&vcount_in>=100&&vcount_in<=150)rgb_out <= WALL_COLOUR;
                   else if(hcount_in<=305&&hcount_in>=300&&vcount_in>=150&&vcount_in<=200)rgb_out <= WALL_COLOUR;
                   else if(hcount_in<=355&&hcount_in>=350&&vcount_in>=150&&vcount_in<=200)rgb_out <= WALL_COLOUR;
                   else if(hcount_in<=355&&hcount_in>=350&&vcount_in>=100&&vcount_in<=150)rgb_out <= WALL_COLOUR;
                   else if(hcount_in<=405&&hcount_in>=400&&vcount_in>=150&&vcount_in<=200)rgb_out <= WALL_COLOUR;
                   else if(hcount_in<=405&&hcount_in>=400&&vcount_in>=100&&vcount_in<=150)rgb_out <= WALL_COLOUR;
                   else if(hcount_in<=455&&hcount_in>=450&&vcount_in>=100&&vcount_in<=150)rgb_out <= WALL_COLOUR;
                   else if(hcount_in<=505&&hcount_in>=500&&vcount_in>=100&&vcount_in<=150)rgb_out <= WALL_COLOUR;
                   else if(hcount_in<=555&&hcount_in>=550&&vcount_in>=100&&vcount_in<=150)rgb_out <= WALL_COLOUR;
                   else if(hcount_in<=605&&hcount_in>=600&&vcount_in>=150&&vcount_in<=200)rgb_out <= WALL_COLOUR;
                   else if(hcount_in<=605&&hcount_in>=600&&vcount_in>=100&&vcount_in<=150)rgb_out <= WALL_COLOUR;
                   else if(hcount_in<=655&&hcount_in>=650&&vcount_in>=150&&vcount_in<=200)rgb_out <= WALL_COLOUR;
                   else if(hcount_in<=655&&hcount_in>=650&&vcount_in>=100&&vcount_in<=150)rgb_out <= WALL_COLOUR;
                   else if(hcount_in<=505&&hcount_in>=500&&vcount_in>=100&&vcount_in<=150)rgb_out <= WALL_COLOUR;
                   else if(vcount_in>=(hcount_in+hcount_in -1110)&&vcount_in<=(hcount_in+hcount_in -1100)&&vcount_in>=100&&vcount_in<=200) rgb_out <= WALL_COLOUR;//N
                   else if(vcount_in>=(hcount_in -260)&&vcount_in<=(hcount_in -250)&&vcount_in>=150&&vcount_in<=200) rgb_out <= WALL_COLOUR; //R
                   else if(hcount_in<=530&&hcount_in>=525&&vcount_in>=150&&vcount_in<=200)rgb_out <= WALL_COLOUR; //Y
                   else if(hcount_in<=730&&hcount_in>=725&&vcount_in>=100&&vcount_in<=200)rgb_out <= WALL_COLOUR; //T
                   
                   else rgb_out <= BG_COLOUR;
                   ypos<= 800;
                   xpos<= 800;
                   
                   ball_init_x <= 800;
                   ball_init_y <= 800;
               end
               4'b0001: begin
                 if(hcount_in<=100&&hcount_in>=0&&vcount_in>=100&&vcount_in<=105)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=350&&hcount_in>=250&&vcount_in>=200&&vcount_in<=205)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=800&&hcount_in>=700&&vcount_in>=300&&vcount_in<=305)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=525&&hcount_in>=425&&vcount_in>=400&&vcount_in<=405)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=400&&hcount_in>=300&&vcount_in>=500&&vcount_in<=505)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=800&&hcount_in>=700&&vcount_in>=500&&vcount_in<=505)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=530&&hcount_in>=525&&vcount_in>=0&&vcount_in<=100)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=530&&hcount_in>=525&&vcount_in>=100&&vcount_in<=200)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=105&&hcount_in>=100&&vcount_in>=100&&vcount_in<=200)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=255&&hcount_in>=250&&vcount_in>=300&&vcount_in<=400)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=155&&hcount_in>=150&&vcount_in>=400&&vcount_in<=500)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=530&&hcount_in>=525&&vcount_in>=500&&vcount_in<=600)rgb_out <= WALL_COLOUR;
               else rgb_out <= BG_COLOUR;
                    
                    
                   ypos<= 508;
                   xpos<=700;             
                   
                   ball_init_x <= 105;
                   ball_init_y <= 125;  
                                  
               end
               4'b0010: begin
               if(hcount_in<=400&&hcount_in>=300&&vcount_in>=100&&vcount_in<=105)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=400&&hcount_in>=300&&vcount_in>=300&&vcount_in<=305)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=650&&hcount_in>=550&&vcount_in>=200&&vcount_in<=205)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=500&&hcount_in>=400&&vcount_in>=300&&vcount_in<=305)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=800&&hcount_in>=700&&vcount_in>=300&&vcount_in<=305)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=800&&hcount_in>=700&&vcount_in>=500&&vcount_in<=505)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=100&&hcount_in>=0&&vcount_in>=400&&vcount_in<=405)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=155&&hcount_in>=150&&vcount_in>=0&&vcount_in<=100)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=655&&hcount_in>=650&&vcount_in>=0&&vcount_in<=100)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=155&&hcount_in>=150&&vcount_in>=200&&vcount_in<=300)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=305&&hcount_in>=300&&vcount_in>=200&&vcount_in<=300)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=405&&hcount_in>=400&&vcount_in>=200&&vcount_in<=300)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=405&&hcount_in>=400&&vcount_in>=300&&vcount_in<=400)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=505&&hcount_in>=500&&vcount_in>=300&&vcount_in<=400)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=305&&hcount_in>=300&&vcount_in>=500&&vcount_in<=600)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=405&&hcount_in>=400&&vcount_in>=500&&vcount_in<=600)rgb_out <= WALL_COLOUR;
               else rgb_out <= BG_COLOUR;
               
                     ypos<= 310;
                     xpos<=410; 
                     ball_init_x <= 325;
                     ball_init_y <= 225;
               end
               
               4'b0011: begin
               if(hcount_in<=405&&hcount_in>=300&&vcount_in>=100&&vcount_in<=105)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=800&&hcount_in>=700&&vcount_in>=100&&vcount_in<=105)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=100&&hcount_in>=0&&vcount_in>=200&&vcount_in<=205)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=400&&hcount_in>=300&&vcount_in>=200&&vcount_in<=205)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=800&&hcount_in>=700&&vcount_in>=300&&vcount_in<=305)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=500&&hcount_in>=400&&vcount_in>=400&&vcount_in<=405)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=105&&hcount_in>=0&&vcount_in>=500&&vcount_in<=505)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=405&&hcount_in>=400&&vcount_in>=0&&vcount_in<=100)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=705&&hcount_in>=700&&vcount_in>=200&&vcount_in<=300)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=405&&hcount_in>=400&&vcount_in>=300&&vcount_in<=400)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=105&&hcount_in>=100&&vcount_in>=400&&vcount_in<=500)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=265&&hcount_in>=260&&vcount_in>=500&&vcount_in<=600)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=505&&hcount_in>=500&&vcount_in>=500&&vcount_in<=600)rgb_out <= WALL_COLOUR;
               else if(hcount_in<=705&&hcount_in>=700&&vcount_in>=500&&vcount_in<=600)rgb_out <= WALL_COLOUR;
               else rgb_out <= BG_COLOUR;
               
                     ypos<= 209;
                     xpos<=707; 
                     ball_init_x <= 15;
                     ball_init_y <= 300;
                   end
                   
                   
                4'b0100: begin    
                    if(hcount_in<=290&&hcount_in>=250&&vcount_in>=200&&vcount_in<=205)rgb_out <= WALL_COLOUR;
                    else if(hcount_in<=290&&hcount_in>=250&&vcount_in>=100&&vcount_in<=105)rgb_out <= WALL_COLOUR;
                    else if(hcount_in<=290&&hcount_in>=250&&vcount_in>=150&&vcount_in<=155)rgb_out <= WALL_COLOUR;
                    else if(hcount_in<=250&&hcount_in>=245&&vcount_in>=100&&vcount_in<=205)rgb_out <= WALL_COLOUR; //E
                    else if(hcount_in<=315&&hcount_in>=310&&vcount_in>=100&&vcount_in<=205)rgb_out <= WALL_COLOUR;
                    else if(hcount_in<=367&&hcount_in>=362&&vcount_in>=100&&vcount_in<=205)rgb_out <= WALL_COLOUR; 
                    else if(vcount_in>=(hcount_in+hcount_in -535)&&vcount_in<=(hcount_in+hcount_in -525)&&vcount_in>=100&&vcount_in<=200) rgb_out <= WALL_COLOUR;//N
                    else if(hcount_in<=390&&hcount_in>=385&&vcount_in>=100&&vcount_in<=205)rgb_out <= WALL_COLOUR;
                    else if(hcount_in<=415&&hcount_in>=390&&vcount_in>=100&&vcount_in<=105)rgb_out <= WALL_COLOUR; 
                    else if(vcount_in>=(hcount_in+hcount_in -735)&&vcount_in<=(hcount_in+hcount_in -725)&&vcount_in>=100&&vcount_in<=125) rgb_out <= WALL_COLOUR;
                    else if(hcount_in<=430&&hcount_in>=390&&vcount_in>=200&&vcount_in<=205)rgb_out <= WALL_COLOUR;
                    else if(hcount_in<=430&&hcount_in>=425&&vcount_in>=125&&vcount_in<=205)rgb_out <= WALL_COLOUR; //D
                    else rgb_out <= BG_COLOUR;
                    
                    ypos<= 800;
                    xpos<= 800; 
                    ball_init_x <= 800;
                    ball_init_y <= 800;
               end    
               default: begin
                    if(hcount_in<=290&&hcount_in>=250&&vcount_in>=200&&vcount_in<=205)rgb_out <= WALL_COLOUR;
                    else if(hcount_in<=290&&hcount_in>=250&&vcount_in>=100&&vcount_in<=105)rgb_out <= WALL_COLOUR;
                    else if(hcount_in<=290&&hcount_in>=250&&vcount_in>=150&&vcount_in<=155)rgb_out <= WALL_COLOUR;
                    else if(hcount_in<=250&&hcount_in>=245&&vcount_in>=100&&vcount_in<=205)rgb_out <= WALL_COLOUR; //E
                    else if(hcount_in<=315&&hcount_in>=310&&vcount_in>=100&&vcount_in<=205)rgb_out <= WALL_COLOUR;
                    else if(hcount_in<=367&&hcount_in>=362&&vcount_in>=100&&vcount_in<=205)rgb_out <= WALL_COLOUR; 
                    else if(vcount_in>=(hcount_in+hcount_in -535)&&vcount_in<=(hcount_in+hcount_in -525)&&vcount_in>=100&&vcount_in<=200) rgb_out <= WALL_COLOUR;//N
                    else if(hcount_in<=390&&hcount_in>=385&&vcount_in>=100&&vcount_in<=205)rgb_out <= WALL_COLOUR;
                    else if(hcount_in<=415&&hcount_in>=390&&vcount_in>=100&&vcount_in<=105)rgb_out <= WALL_COLOUR; 
                    else if(vcount_in>=(hcount_in+hcount_in -735)&&vcount_in<=(hcount_in+hcount_in -725)&&vcount_in>=100&&vcount_in<=125) rgb_out <= WALL_COLOUR;
                    else if(hcount_in<=430&&hcount_in>=390&&vcount_in>=200&&vcount_in<=205)rgb_out <= WALL_COLOUR;
                    else if(hcount_in<=430&&hcount_in>=425&&vcount_in>=125&&vcount_in<=205)rgb_out <= WALL_COLOUR; //D
                    else rgb_out <= BG_COLOUR;
                    
                    ypos<= 800;
                    xpos<= 800; 
                    ball_init_x <= 800;
                    ball_init_y <= 800;
              end      
              endcase  
          
       end
           
       vcount_out<=vcount_in;
       hcount_out<=hcount_in;
       vsync_out<=vsync_in;
       vblnk_out<=vblnk_in;
       hblnk_out<=hblnk_out;
       hsync_out<=hsync_in;
      
       level_out<=level_in;
           
      
    end
    
      
    
endmodule
