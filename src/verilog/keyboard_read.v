`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 13.07.2019 13:25:54
// Design Name: 
// Module Name: fifo_read
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module keyboard_read(
    input wire clk,
    input wire reset,
    input wire flag,
    input wire [7:0] r_data,
        
    output reg [2:0] arrow_out,    
    output reg disp_clr,
    output wire read_out
);     
    
    localparam UP=3'b001, DOWN=3'b010, LEFT=3'b011, RIGHT=3'b100, R=3'b101;
    
    
    
    reg [7:0] reg_one;
   
    always @(posedge clk) begin
      if(reset) begin 
            arrow_out <= 0;
            disp_clr <= 0;
      end
      else begin 
        
            if  (flag)   begin   

                if (r_data == 8'h75)               
                    arrow_out<=UP;
                else if (r_data == 8'h72)               
                    arrow_out<=DOWN;
                else if (r_data == 8'hE0)               
                    arrow_out<=RIGHT;
                else if (r_data == 8'h6B)               
                    arrow_out<=LEFT;                
                else if(r_data == 8'h2D)
                    arrow_out<=R;
                else
                    arrow_out<=8'hFF;
                    
                if(r_data == 8'h29)
                    disp_clr<=1;
                else
                    disp_clr<=0;      

            end
            else 
                arrow_out<=0;  
       end
    end
    assign read_out = arrow_out;
  
endmodule
