`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05.03.2019 14:49:59
// Design Name: 
// Module Name: draw_background
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

 module draw_ball(

    input wire [10:0] hcount_in,
    input wire hblnk_in,
    input wire hsync_in,
    input wire [10:0] vcount_in,
    input wire vblnk_in,
    input wire vsync_in, 
    input wire pclk,
    input wire rst,
    input wire [11:0] rgb_in,
    input wire [11:0] xpos,
    input wire [11:0] ypos,
    input wire [11:0] rgb_pixel,

    output reg [10:0] hcount_out,
    output reg hblnk_out,
    output reg hsync_out,
    output reg [10:0] vcount_out,
    output reg vsync_out,
    output reg vblnk_out,
    output reg [11:0] rgb_out,
    output wire [11:0] rgb_addr
    
    );
    
    wire [5:0] addrx, addry;
    reg hsync_nxt, vsync_nxt, hblnk_nxt, vblnk_nxt;
    reg [10:0] hcount_nxt, vcount_nxt;
    reg [11:0] rgb_nxt;
    reg hsync_temp, vsync_temp, hblnk_temp, vblnk_temp;
    reg [10:0] hcount_temp, vcount_temp;
    reg [11:0] rgb_temp;
  
    localparam SIZE=50, COLOUR= 12'h0_0_f;
    
always @* begin 
           
     vcount_nxt=vcount_temp;
     hcount_nxt=hcount_temp;
     vsync_nxt=vsync_temp;
     vblnk_nxt=vblnk_temp;
     hblnk_nxt=hblnk_temp;
     hsync_nxt=hsync_temp;
     rgb_nxt=rgb_temp;
       
     
     if( hcount_nxt>=xpos && hcount_nxt<xpos+SIZE && vcount_nxt>=ypos && vcount_nxt<ypos+SIZE && rgb_pixel!=12'hccc ) 
         rgb_nxt = rgb_pixel; 
     else 
         rgb_nxt=rgb_in;
end     
  
always @(posedge pclk)
     
     if (rst) begin                 
               
         hsync_out <= 0;           
         vsync_out <= 0;           
         hblnk_out <= 0;           
         vblnk_out <= 0;           
         hcount_out <= 0;          
         vcount_out <= 0;     
         rgb_out<=0;   
        
               
    end       
    else   begin
    
         vcount_temp<=vcount_in;
         hcount_temp<=hcount_in;               
         vsync_temp<=vsync_in;                   
         vblnk_temp<=vblnk_in;                   
         hblnk_temp<=hblnk_in;                   
         hsync_temp<=hsync_in;                   
                 
         vcount_out<=vcount_nxt;
         hcount_out<=hcount_nxt;
         vsync_out<=vsync_nxt;
         vblnk_out<=vblnk_nxt;
         hblnk_out<=hblnk_nxt;
         hsync_out<=hsync_nxt;
         rgb_out<=rgb_nxt;
    
    
    end
               
    assign addry = vcount_in - ypos;                     
    assign addrx = hcount_in - xpos;                     
    assign rgb_addr = SIZE*addry+addrx;            
    
endmodule