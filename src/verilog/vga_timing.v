// File: vga_timing.v
// This is the vga timing design for EE178 Lab #4.

// The `timescale directive specifies what the
// simulation time units are (1 ns here) and what
// the simulator time step should be (1 ps here).

`timescale 1 ns / 1 ps


// Declare the module and its ports. This is
// using Verilog-2001 syntax.

module vga_timing (
  output reg[10:0] vcount=0, 
  output reg vsync,        
  output reg vblnk,         
  
  output reg [10:0] hcount=0, 
  output reg hsync,
  output reg hblnk,
  input wire pclk,
  input wire rst

  );
   
 
  // Describe the actual circuit for the assignment.
  // Video timing controller set for 800x600@60fps
  // using a 40 MHz pixel clock per VESA spec.
 
 
  localparam H_TOTAL_TIME = 1055;
  localparam H_SYNC_START = 839;
  localparam H_SYNC_END  =  967;
  localparam H_BLANK_START = 799;
  localparam V_TOTAL_TIME = 627;
  localparam V_BLANK_START = 599;
  localparam V_SYNC_START = 600;
  localparam V_SYNC_END  =  604; 
 
 

  
    
    always@ (posedge pclk) 
  
     if(rst) begin
          vcount<=0;
          hcount<=0;
          vsync<=0;
          vblnk<=0;
          hblnk<=0;
          hsync<=0;
      end
      else begin
  
         hcount <= hcount + 1;  // horizontal counter
        
         if  (H_BLANK_START <= hcount  && H_TOTAL_TIME > hcount) begin
                   
                hblnk <= 1;
              if  ( H_SYNC_START <= hcount && H_SYNC_END >  hcount)
                     hsync <= 1;           
                 else
                     hsync <= 0;           
         end
         else if (H_TOTAL_TIME == hcount ) begin      
                 hcount <= 0;
                 hblnk <= 0;           
                 vcount <= vcount + 1; //vertical counter
                 
                 if (V_BLANK_START <= vcount   && V_TOTAL_TIME > vcount  )begin
                 
                     vblnk <= 1;             
                     if (  V_SYNC_START <=vcount   && V_SYNC_END > vcount )
                         vsync <= 1;           
                     else
                         vsync <= 0;           
                 end
                 
                 else if ( V_TOTAL_TIME == vcount ) begin
                     vcount <= 0;
                     vblnk <= 0;
                 end
             end
          end
        
  
  endmodule