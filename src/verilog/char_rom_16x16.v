`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.04.2019 22:39:01
// Design Name: 
// Module Name: char_rom_16x16
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module char_rom_16x16(
    input wire [9:0] char_xy,
    output reg [6:0] char_code,
    input wire clk
    );
    localparam SIZE=64;
    reg [8*SIZE-1:0] txt;
    reg [6:0] data;
    integer i=0;

    // body
    always @(posedge clk)
        char_code <= data;

    always @* begin
        if (char_xy[9])
            txt="Press spacebar to start, use   arrows to move and r to restart  ";
        else
            txt="           Level done              Press spacebar to continue   ";     
        
        for (i=0; i<=6 ; i=i+1)begin
            data[i]=txt[8*SIZE-8*char_xy+i];        
        end
        
    end
endmodule