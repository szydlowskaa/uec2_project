`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04.08.2019 11:02:37
// Design Name: 
// Module Name: ball
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ball_control (
    input wire clk,
    input wire rst,
    input wire [2:0] arrow_in,   
    input wire disp_clr,
    //input wire [3:0] level_in,
    
    output wire [3:0] level_out,
    output wire level_done,
    output wire [10:0] xpos,
    output wire [10:0] ypos   
    );
    
 wire [10:0] x_pixel, y_pixel;
 wire [11:0] rgb_xy; 
 wire [3:0] level_in;
 
    
 draw_background my_background_ctl(
       
   .vcount_in(y_pixel),    
   .vsync_in(0),       
   .vblnk_in(0),       
   .hcount_in(x_pixel),     
   .hsync_in(0),       
   .hblnk_in(0),       
   .pclk(clk),            
   .rst(rst), 
   .rgb_pixel(12'h002), 
   .level_in(level_out),          
   
   .vcount_out(),
   .vsync_out(),
   .vblnk_out(),
   .hcount_out(),
   .hsync_out(),
   .hblnk_out(),
   .rgb_out(rgb_xy),
   //.rgb_addr(),
   .level_out(level_in)
   );   
   
   
   draw_ball_ctl my_draw_ball_ctl(
     
     .rst(rst),
     .clk(clk),
     .arrow_in(arrow_in),  
     .rgb_in(rgb_xy),  
     .disp_clr(disp_clr),
     .level_in(level_in),
     
     .x_pixel(x_pixel),
     .y_pixel(y_pixel),
     .xpos(xpos),   
     .level_done(level_done),
     .level_out(level_out),              
     .ypos(ypos)
    
   );  
   
    
endmodule
