`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04.08.2019 13:20:59
// Design Name: 
// Module Name: message
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module message(

   
    input wire [10:0] hcount_in,              
    input wire [10:0] vcount_in,
    input wire pclk,                   
    input wire rst,                    
    input wire [11:0] rgb_in,
    input wire [1:0] level_done,
    input wire  disp_clr,
        
    output wire [11:0] rgb_out

    );
    
    
    wire [3:0] char_line; 
    wire [9:0] char_xy;
    wire [7:0] char_pixels;  
    wire [10:0] addr_char;
    
     
  draw_message_char my_draw_message_char(
            
        .vcount_in(vcount_in),  
        .hcount_in(hcount_in),         
        .rgb_in(rgb_in),                
        .rst(rst),                         
        .pclk(pclk),  
        .level_done(level_done), 
        .disp_clr(disp_clr),
        .char_pixels(char_pixels),
        .rgb_out(rgb_out),
        .char_xy(char_xy),
        .char_line(addr_char[3:0])
        
      );
      
  font_rom my_font_rom(
  
        .clk(pclk),
        .addr(addr_char),
        
        .char_line_pixels(char_pixels)
             
   );
        
  char_rom_16x16 my_char_rom_16x16(
        .clk(pclk),
        .char_xy(char_xy),
        
        .char_code(addr_char[10:4])        
   );
         
    
    
    
endmodule
