`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.04.2019 15:06:58
// Design Name: 
// Module Name: draw_rect_ctl
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module draw_ball_ctl(

    input wire clk,
    input wire rst,
    input wire [2:0] arrow_in,
    input wire [11:0] rgb_in,
    input wire disp_clr,
    input wire [3:0] level_in,
    input wire [11:0] ball_init_x,ball_init_y,
    
    
    output reg [1:0] level_done,
    output wire [3:0] level_out,
    output reg [10:0] x_pixel,
    output reg [10:0] y_pixel,   
    output reg [11:0] xpos,
    output reg [11:0] ypos
    );
    
    reg [2:0] state,  nxt_state, v;
    reg [11:0] xpos_nxt, ypos_nxt;   

    reg [18:0] counter_hp, counter_hp_nxt;
    reg [3:0] state_ctl, state_ctl_nxt;
    reg [3:0] nxt_level;
    reg [1:0] level_done_nxt;
    
    localparam HEIGHT = 50, IDLE = 3'b000, UP=3'b001, DOWN=3'b010, LEFT=3'b011, RIGHT=3'b100, R=3'b101, START=3'b111, END_DOWN=599, END_RIGHT=799, WIDTH=50;
    localparam OUT = 800;
    localparam WALL_COLOUR=12'hf_f_f, EXIT=12'h002;
    
    always @(posedge clk)    
      if(rst) begin
            state <= 0;
            xpos <= 0;
            ypos <= 0;
            state <= START;          
            counter_hp <= 0;
          
                
            
                 
      end
      else begin
            state <= nxt_state;
            ypos <=   ypos_nxt;
            xpos <=   xpos_nxt; 
            state_ctl<=state_ctl_nxt;           
            level_done<=level_done_nxt;
                         
            if(counter_hp<=100000) begin
               counter_hp<=counter_hp+1;   
               v<=0;
            end   
            else begin
               counter_hp<=0;      
               v <=1;           
            end  
         
      end
      
        
 always @* begin
      
    case(state)
        
       START: begin
   
            nxt_level=level_in;  
            if(level_in==4'b0000||level_in==4'b0100)begin
                xpos_nxt = 799;
                ypos_nxt = 599;
            end
            else begin   
                xpos_nxt = ball_init_x;
                ypos_nxt = ball_init_y;
            end 
                
            nxt_state=IDLE;
            state_ctl_nxt=START;
            x_pixel =OUT;
            y_pixel =OUT;
            
       end 
       
       DOWN: begin
            
            xpos_nxt =xpos;
            state_ctl_nxt=DOWN;
            x_pixel = xpos_nxt+WIDTH/2; 
            nxt_level=level_in;
            
           if (rgb_in==WALL_COLOUR)begin
              nxt_state=IDLE;
              ypos_nxt=ypos;
            end  
            else begin
               nxt_state=DOWN;              
               ypos_nxt=ypos+v;
               
            end            
            y_pixel =ypos_nxt+HEIGHT; 
       end   
       UP: begin
                                              
            xpos_nxt =xpos;
            state_ctl_nxt=UP;
            x_pixel = xpos_nxt+WIDTH/2; 
            nxt_level=level_in;
 
            if(rgb_in==WALL_COLOUR)begin
               nxt_state=IDLE;
               ypos_nxt=ypos;
            end  
            else begin
               nxt_state=UP;
               ypos_nxt=ypos-v;
            end
            y_pixel =ypos_nxt-1;
       end    
  
                                    
       RIGHT: begin
                              
            ypos_nxt =ypos;  
            state_ctl_nxt=RIGHT;
            y_pixel =ypos+HEIGHT/2; 
            nxt_level=level_in;
                  
            if (rgb_in==WALL_COLOUR)begin
              nxt_state=IDLE;
              xpos_nxt=xpos;         
            end  
            else begin               
               nxt_state=RIGHT;
               xpos_nxt=xpos+v;
            end   
            x_pixel = xpos_nxt+WIDTH+1;  
       end   
       
       
       LEFT: begin
                                                   
            ypos_nxt =ypos;  
            state_ctl_nxt=LEFT;           
            y_pixel =ypos_nxt+HEIGHT/2; 
            nxt_level=level_in;
                 
            if(rgb_in==WALL_COLOUR)begin
               nxt_state=IDLE;
               xpos_nxt=xpos;                            
            end  
            else begin
               nxt_state=LEFT;
               xpos_nxt=xpos-v; 
            end     
            x_pixel = xpos_nxt;               
       end            
          
                    
       IDLE: begin
            if ((disp_clr&&rgb_in==EXIT) || (level_in==0&&disp_clr) )begin
                nxt_state=START;
                nxt_level=level_in+1;
            end  
            else begin      
       
        
                if(arrow_in==R)
                    nxt_state=START;
                else if (arrow_in==UP && state_ctl!=UP&&level_in!=0)
                    nxt_state=UP;
                else if(arrow_in==DOWN && state_ctl!=DOWN&&level_in!=0)
                     nxt_state=DOWN; 
                else if (arrow_in==RIGHT && state_ctl!=RIGHT&&level_in!=0)
                    nxt_state=RIGHT;  
                else if(arrow_in==LEFT && state_ctl!=LEFT&&level_in!=0 )
                    nxt_state=LEFT;             
                else               
                    nxt_state=IDLE;
                    
                nxt_level=level_in;     
             end    
           
            ypos_nxt=ypos;
            xpos_nxt=xpos;
            state_ctl_nxt=state_ctl;
            x_pixel =xpos_nxt+WIDTH/2;
            y_pixel =ypos_nxt+HEIGHT/2;
            
       end
       
      
       default: begin
            nxt_state=IDLE;
            xpos_nxt =xpos;
            ypos_nxt=ypos;                   
            counter_hp_nxt=0;
            state_ctl_nxt=START;
            x_pixel =OUT;
            y_pixel =OUT;
            nxt_level=level_in; 
            
       end
    endcase        
        if (rgb_in==EXIT )
            level_done_nxt=2'b01;
        else if (nxt_level==0)
            level_done_nxt=2'b11;    
        else 
            level_done_nxt=2'b00;                        
    
 end
    
    assign level_out=nxt_level;
            
endmodule