`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 20.08.2019 12:55:43
// Design Name: 
// Module Name: ball
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ball(

    input wire [10:0] hcount_in,
    input wire hblnk_in,
    input wire hsync_in,
    input wire [10:0] vcount_in,
    input wire vblnk_in,
    input wire vsync_in, 
    input wire pclk,
    input wire rst,
    input wire [11:0] rgb_in,
    input wire [11:0] xpos,
    input wire [11:0] ypos,

    output wire [10:0] hcount_out,
    output wire hblnk_out,
    output wire hsync_out,
    output wire [10:0] vcount_out,
    output wire vsync_out,
    output wire vblnk_out,
    output wire [11:0] rgb_out

    );
    
    wire [11:0] rgb_pixel;
    wire [11:0] rgb_addr;
    
   draw_ball my_draw_ball(
        
        .vcount_in(vcount_in),   
        .vsync_in(vsync_in),           
        .vblnk_in(vblnk_in),           
        .hcount_in(hcount_in),         
        .hsync_in(hsync_in),           
        .hblnk_in(hblnk_in),           
        .rgb_in(rgb_in),                
        .rst(rst),                         
        .pclk(pclk),                       
        .xpos(xpos),                  
        .ypos(ypos),
        .rgb_pixel(rgb_pixel),
                                     
        .vcount_out(vcount_out),      
        .vsync_out(vsync_out),        
        .vblnk_out(vblnk_out),        
        .hcount_out(hcount_out),      
        .hsync_out(hsync_out),        
        .hblnk_out(hblnk_out),        
        .rgb_out(rgb_out),
        .rgb_addr(rgb_addr)  
             
   );
      
   image_ball my_image_ball(
       
        .clk(pclk),
        .address(rgb_addr),
        
        .rgb(rgb_pixel)  
     
    );
         
endmodule
